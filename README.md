# Web Push Notifications Demo using Flask on Desktop and Mobile

Tested in chrome  
In Brave or other browsers you may need to set some flags in settings

### Init backend
```
cp example.env .env
python3 -m venv venv
source venv/bin/activate
# pip3 freeze > requirements.txt
pip3 install -r requirements.txt
python main.py
```

### How to generate vapid keys
```
openssl ecparam -name prime256v1 -genkey -noout -out vapid_private.pem
openssl ec -in ./vapid_private.pem -outform DER|tail -c +8|head -c 32|base64|tr -d '=' |tr '/+' '_-' >> private_key.txt
openssl ec -in ./vapid_private.pem -pubout -outform DER|tail -c 65|base64|tr -d '=' |tr '/+' '_-' >> public_key.txt
```

### Common problems
```
# On android, chrome doesn't receive push notifications
Check if Google Play Services installed on the phone,
and if it's not disabled and have access to the internet
```

### Tips and tricks
You can get push messages faster and more reliable if  
you set 99 to your domain in chrome browser  
about://site-engagement/  

### How to send push notifications via postman
When you open this deployed tutorial via chrome on your smartphone and subscribe to notifications,  
in the terminal through which you started the server you'll see debug messages, among them you'll see the entry:  
```
subscription {
  endpoint: 'https://fcm.googleapis.com/fcm/send/c062eVL....
```
using these credentials (endpoint, p256dh, auth) you can replicate the push notification  
open postman and create a new tab  
select POST  
paste into URL 
```
https://yourdomain.com/api/notify  
```
in Headers set  
```
Content-Type: application/json; charset=utf-8  
```
Body > raw  
```
{"subscription":{"endpoint":"https://fcm.googleapis.com/fcm/send/c062eVLfj0k:APA91bHHhQvNHIlbZX7b_rcXFZ_5oDknZcZHCB...........","expirationTime":null,"keys":{"p256dh":"BFf9K8oAqqdG6RXseO9n46JObJsz.......","auth":"pS5b..........."}},"payload":"{\"title\":\"Testing push messages\",\"message\":\"Click on this notification\"}","delay":1000}
```
Response:  
```
{
    "statusCode": 201,
    "body": "",
    "headers": {
        "location": "https://fcm.googleapis.com/0:16.......................",
        "x-content-type-options": "nosniff",
        "x-frame-options": "SAMEORIGIN",
        "x-xss-protection": "0",
        "date": "Fri, 1 Jan 2023 10:00:00 GMT",
        "content-length": "0",
        "content-type": "text/html; charset=UTF-8",
        "alt-svc": "h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"",
        "connection": "close"
    }
}
```

