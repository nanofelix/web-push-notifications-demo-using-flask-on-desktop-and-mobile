function apiGetKey() {
    return fetch('./api/key/')
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            return urlB64ToUint8Array(data.key);
        })
}

function apiSendPush(sub, title, message, delay) {
    const data = {
        subscription: sub,
        payload: JSON.stringify({
            title: title,
            message: message
        })
    }

    if (delay) {
        data.delay = delay;
    }

    return fetch('./api/notify/', {
        method: 'post',
        headers: {
            'Content-type': 'application/json'
        },
        body: JSON.stringify(data)
    });
}
