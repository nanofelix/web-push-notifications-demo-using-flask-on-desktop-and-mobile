import logging
import json, os

from flask import request, Response, render_template, jsonify, Flask
from pywebpush import webpush, WebPushException

from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

app = Flask(__name__)
app.config['SECRET_KEY'] = '9OLWxND4o83j4K4iuopO'

VAPID_PUBLIC_KEY = os.environ.get("ENV_VAPID_PUBLIC_KEY")
VAPID_PRIVATE_KEY = os.environ.get("ENV_VAPID_PRIVATE_KEY")

VAPID_CLAIMS = {
"sub": "mailto:test@some.in"
}

def send_web_push(subscription_information, message_body):
    return webpush(
        subscription_info=subscription_information,
        data=message_body,
        vapid_private_key=VAPID_PRIVATE_KEY,
        vapid_claims=VAPID_CLAIMS
    )

@app.route('/')
def index():
    return render_template('index.html')

@app.route("/api/key/", methods=["GET", "POST"])
def get_key():
    """
        returns vapid public key which clients uses to send around push notification
    """
    if request.method == "GET":
        return Response(response=json.dumps({"key": VAPID_PUBLIC_KEY}),
            headers={"Access-Control-Allow-Origin": "*"}, content_type="application/json")

@app.route("/api/notify/",methods=['POST'])
def notify():
    if not request.json or not request.json.get('subscription'):
        return jsonify({'failed':1})

    subscription = request.json.get('subscription')
    payload = request.json.get('payload')

    """     
    title = "This is title"
    message = "Push Something Test v1"
    payload = json.dumps({'title': title, 'message': message})
    """

    try:
        send_web_push(subscription, payload)
        return jsonify({'success':1})
    except Exception as e:
        print("error",e)
        return jsonify({'failed':str(e)})

if __name__ == "__main__":
    app.run(host="0.0.0.0",port=80)
